﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// use an enum to describe player name
public enum PlayerInputManager
{
    Player1,
    Player2
}

public class PlayerMove : MonoBehaviour {
	public Vector2 move;
	public Vector2 velocity; // in metres per second
    public PlayerInputManager playerInput;

    // W03code
    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 3.0f; // in metres/second/second
    private float speed = 0.0f;    // in metres/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second
	public float maxTurnSpeed = 30.0f;


    private BeeSpawner beeSpawner;
    public float destroyRadius = 1.0f;

    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        // draw ray from bee to player one
        Gizmos.DrawWireSphere(transform.position, destroyRadius);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }

      

        /*
		// get the input values
		Vector2 direction = new Vector2();

        if (playerInput == PlayerInputManager.Player1)
        {
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");

        } else if (playerInput == PlayerInputManager.Player2)
        {
            direction.x = Input.GetAxis("Horizontal2");
            direction.y = Input.GetAxis("Vertical2");
        }
        */

        // the horizontal axis controls the turn
        float turn = Input.GetAxis("Horizontal");



        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            // bug: the speed will never go to zero --> the card will never stop (actually)
            // simple explain: 
            // brake * Time.deltaTime will never equal to speed, 
            // then the speed will change between positive and negative values (called it Range [A,B])
            // more explanation : 
            // Time.deltaTime returns the time (in second) which need to complete the last frame
            // the stats when playing game shows that about 80 frames per second
            // it takes about 1000ms for 80 frames
            // Time.deltaTime is about 10-12ms per frame = 0.01-0.012
            // brake * Time.deltaTime cann't be exact value of speed to make zero speed
           
            
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
            }
            else if (speed < 0)
            {
                speed = speed + brake * Time.deltaTime;
            }

            // fix: if the absolute value of speed is less than a constant value, let's say 0.01
            // set the speed to zero
            if (Mathf.Abs(speed) < 0.01)
                speed = 0;
        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;


		// turn the car
		// bug: reverse rotation
		// Quaternion.Euler(0, 0, Mathf.Lerp(0,zAngleTarget, t));

		// if speed is close to zero that means the turn speed is close to maximum (eg. 30 degree per second)
		// if speed is close to max speed, then the turn speed is close to half of maximum
		float proportion = Mathf.Abs(speed / maxSpeed);
		if (proportion > 0.5)
			turnSpeed = maxTurnSpeed / 2;
		else
			turnSpeed = maxTurnSpeed;

		float zAngle = turn * turnSpeed * Time.deltaTime * -1;
		transform.Rotate(0, 0, zAngle);

        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);

    }
}
