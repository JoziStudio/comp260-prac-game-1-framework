﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
    public BeeMove beePrefab;
    public PlayerMove player;
    public int nBees = 4;
    public float xMin, yMin;
    public float width, height;
    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;

    // code for week 4
    public float beePeriod = 1f;
    public float minBeePeriod = 0.5f;
    public float maxBeePeriod = 2f;
    public bool useRandomPeriod = false;
    public int countBee = 0;
    private float timer;

    // private state
    private float speed;
    private float turnSpeed;
    private Transform target;
    private Vector2 heading;

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
				countBee--;
				if (countBee < 0)
					countBee = 0;
            }


        }
    }

    void SpawnBee(string name)
    {

        // instantiate a bee
        BeeMove bee = Instantiate(beePrefab);
        // attach to this object in the hierarchy
        bee.transform.parent = transform;
        // give the bee a name and number
        bee.gameObject.name = "Bee " + name;

        // move the bee to a random position within 
        // the bounding rectangle
        float x = xMin + Random.value * width;
        float y = yMin + Random.value * height;
        bee.transform.position = new Vector2(x, y);
    }


    // Use this for initialization
    void Start () {
        // for week 4
        if (!useRandomPeriod)
            timer = beePeriod;
        else
            timer = Random.Range(minBeePeriod, maxBeePeriod);
        // end

        // find a player object to be the target by type
        player = (PlayerMove)FindObjectOfType(typeof(PlayerMove));
        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
                                 Random.value);



    }

    // Update is called once per frame
    void Update () {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            // reset timer 
            if (!useRandomPeriod)
                timer = beePeriod;
            else
                timer = Random.Range(minBeePeriod, maxBeePeriod);

            // spawn a bee if it's available to spawn
            // otherwise, wait to the next period
            if (countBee < nBees)
            {
                countBee++;
                SpawnBee(countBee.ToString());
            }
        }
	}
}
